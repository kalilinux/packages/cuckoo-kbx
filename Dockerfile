FROM debian:stable-slim
ARG KBX_APP_VERSION=2.0.7
RUN apt-get update && apt-get install -y \
    git \
    python-pip \
    tcpdump \
    libcap2-bin \
    libfreetype6-dev \
    libjpeg-dev \
    zlib1g-dev \
    liblcms2-dev \
    libtiff5-dev \
    libwebp-dev \
    libraqm-dev \
    libimagequant-dev \
    volatility
RUN pip install Cuckoo==$KBX_APP_VERSION
#RUN git clone -b $KBX_APP_VERSION https://github.com/cuckoosandbox/cuckoo
#RUN mkdir /kaboxer ; echo $KBX_APP_VERSION > /kaboxer/version
#COPY ./kbx-hello /kbx-hello
